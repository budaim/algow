
import Data.List
import Data.Foldable (foldrM)

import Control.Monad
import Control.Monad.State
import Control.Monad.Error
import Control.Monad.Reader

import qualified Data.Set as Set
import qualified Data.Map as Map

import Text.Parsec
import Text.Parsec.Language
import qualified Text.Parsec.Token as T

-- lexer and language definition
languageDef = emptyDef { T.commentStart = "(*"
                       , T.commentEnd   = "*)"
                       , T.commentLine  = "---"
                       , T.identStart   = letter <|> char '_'
                       , T.identLetter  = alphaNum <|> char '_'
                       , T.reservedNames = [ "fun"
                                           , "let"
                                           , "in"
                                           , "="
                                           , "->"
                                           , "("
                                           , ")"
                                           , ","
                                           , "true"
                                           , "false"
                                           ]
                       , T.reservedOpNames = []
                       }

lexer = T.makeTokenParser languageDef
parens = T.parens lexer
whiteSpace = T.whiteSpace lexer
identifier = T.identifier lexer
reserved = T.reserved lexer


-- Parser
data Exp = EVar String
         | EApp Exp Exp
         | ETup [Exp]
         | EFun Exp Exp
         | ELet String Exp Exp
         | EBool
         deriving (Eq, Ord, Show)

parseExp = do
        e <- parseLet <|> parseFun <|> parseSimpleExp
        try whiteSpace
        return e

parseLet = do
        reserved "let"
        ident <- identifier
        reserved "="
        expr1 <- parseExp
        reserved "in"
        expr2 <- parseExp
        return $ ELet ident expr1 expr2

parseFun = do
        reserved "fun"
        params <- many identifier
        reserved "->"
        expr <- parseExp
        return $ EFun (ETup $ map EVar params) expr

parseAtom = try (parens parseExp >>= \x -> notFollowedBy (string "(") >> return x)
        <|> try (identifier >>= \x -> notFollowedBy (string "(") >> (return $ EVar x))

parseBool = (reserved "true" <|> reserved "false") >> return EBool

parseSimpleExp = parseBool <|> parseAtom <|> parseApp

parseApp = do
        atom <- parseAppAtom
        allParams <- many1 parseParams
        return $ foldl EApp atom $ map ETup allParams
    where
        parseAppAtom = parens parseExp <|> (identifier >>= return . EVar)
        parseParams = 
            do { string "("; params <- sepBy parseExp (string ", "); string ")"; return params }

-- W Algorithm

data Type = TVar String
          | TTup [Type]
          | TFun Type Type
          | TBool
          | TInt
          | TList Type
          | TPair Type Type
          deriving (Eq, Ord, Show)

data Scheme = Scheme [String] Type deriving (Show)

class Types a where
        ftv :: a -> Set.Set String
        apply :: Subst -> a -> a

instance Types a => Types [a] where
        ftv l = foldr Set.union Set.empty (map ftv l)
        apply s = map (apply s)

instance Types Type where
        ftv (TVar n) = Set.singleton n
        ftv (TFun a b) = Set.union (ftv a) (ftv b)
        ftv (TTup l) = ftv l
        ftv TBool = Set.empty
        ftv TInt = Set.empty
        ftv (TList t) = ftv t
        ftv (TPair t1 t2) = Set.union (ftv t1) (ftv t2)
        apply s (TVar n) = case Map.lookup n s of
                               Nothing -> TVar n
                               Just t -> t
        apply s (TFun a b) = TFun (apply s a) (apply s b)
        apply s (TTup l) = TTup $ apply s l
        apply s (TList t) = TList (apply s t)
        apply s (TPair t1 t2) = TPair (apply s t1) (apply s t2)
        apply s t = t

instance Types Scheme where
        ftv (Scheme vars t) = (ftv t) Set.\\ (Set.fromList vars)
        apply s (Scheme vars t) = Scheme vars (apply (foldr Map.delete s vars) t)

type Subst = Map.Map String Type

nullSubst = Map.empty
composeSubst a b = (Map.map (apply a) b) `Map.union` a

newtype TypeEnv = TypeEnv (Map.Map String Scheme) deriving (Show)

remove (TypeEnv env) var = TypeEnv (Map.delete var env)

instance Types TypeEnv where
        ftv (TypeEnv env) = ftv (Map.elems env)
        apply s (TypeEnv env) = TypeEnv (Map.map (apply s) env)

generalize env t = Scheme vars t
    where vars = Set.toList ((ftv t) Set.\\ (ftv env))

data TIEnv = TIEnv {}
data TIState = TIState { tiSupply :: Int
                       , tiSubst  :: Subst
                       }

type TI a = ErrorT String (ReaderT TIEnv (StateT TIState IO)) a

runTI :: TI a -> IO (Either String a, TIState)
runTI t = do
        (res,st) <- runStateT (runReaderT (runErrorT t) initTIEnv) initTIState
        return (res,st)
    where
        initTIEnv = TIEnv {}
        initTIState = TIState { tiSupply = 0
                              , tiSubst = Map.empty
                              }

newTyVar :: String -> TI Type
newTyVar prefix = do
        s <- get
        put s { tiSupply = tiSupply s + 1 }
        return (TVar (prefix ++ show (tiSupply s)))

instantiate :: Scheme -> TI Type
instantiate (Scheme vars t) = do
        nvars <- mapM (\_ -> newTyVar "a") vars
        let s = Map.fromList (zip vars nvars)
        return $ apply s t

mgu :: Type -> Type -> TI Subst
mgu (TFun l r) (TFun l' r') = do
        s1 <- mgu l l'
        s2 <- mgu (apply s1 r) (apply s1 r')
        return (s1 `composeSubst` s2)
mgu (TVar u) t = varBind u t
mgu t (TVar u) = varBind u t
mgu (TTup l1) (TTup l2) = do
        x <- foldrM (\(x,y) s -> do
            s' <- mgu (apply s x) (apply s y)
            return $ composeSubst s' s) nullSubst (zip l1 l2)
        return x
mgu TBool TBool = return nullSubst
mgu TInt TInt = return nullSubst
mgu (TList t1) (TList t2) = mgu t1 t2
mgu (TPair ta tb) (TPair tc td) = do
        s1 <- mgu ta tc
        s2 <- mgu tb td
        return $ composeSubst s1 s2
mgu t1 t2 = throwError $ "types do not unify " ++ show t1 ++ " vs " ++ show t2

varBind :: String -> Type -> TI Subst
varBind u t | t == TVar u = return nullSubst
            | u `Set.member` ftv t = throwError $ "occur check fails: " ++ u ++ " vs " ++ show t
            | otherwise = return (Map.singleton u t)

ti :: TypeEnv -> Exp -> TI (Subst, Type)
ti (TypeEnv env) (EVar n) =
        case Map.lookup n env of
            Nothing -> throwError $ "unbound variable: " ++ n
            Just sigma -> do t <- instantiate sigma
                             return (nullSubst,t)
ti _ EBool = return (nullSubst,TBool)
ti evn (ETup []) = return (nullSubst,TTup [])
ti env (ETup [x]) = ti env x
ti env (ETup xs) = do
        w <- mapM (ti env) xs
        let s = foldl Map.union nullSubst $ map fst w
        return (s,TTup $ map snd w)
ti env i@(EApp e1 e2) = do
        tv <- newTyVar "a"
        (s1,t1) <- ti env e1
        (s2,t2) <- ti (apply s1 env) e2
        s3 <- mgu (apply s2 t1) (TFun t2 tv)
        return (s3 `composeSubst` s2 `composeSubst` s1, apply s3 tv)
ti env (EFun (ETup []) e) = do
        (s,t) <- ti env e
        return (s,TFun (TTup []) t)
ti env (EFun (ETup [EVar n]) e) = do
        tv <- newTyVar "a"
        let TypeEnv env' = remove env n
            env'' = TypeEnv (env' `Map.union` (Map.singleton n (Scheme [] tv)))
        (s1,t1) <- ti env'' e
        return (s1,TFun (apply s1 tv) t1)
ti env (EFun (ETup ns) e) = do
        w <- mapM (\(EVar n) -> do
            tv <- newTyVar "a"
            let TypeEnv env' = remove env n
                env'' = TypeEnv (env' `Map.union` (Map.singleton n (Scheme [] tv)))
            return (tv,env'')
            ) ns
        let envs = map snd w
            vars = map fst w
            envs' = TypeEnv (foldl (\a (TypeEnv e) -> Map.union a e) Map.empty envs)
        (s1,t1) <- ti envs' e
        return (s1, TFun (TTup (apply s1 vars)) t1)
ti env (ELet x e1 e2) = do
        (s1,t1) <- ti env e1
        let TypeEnv env' = remove env x
            t' = generalize (apply s1 env) t1
            env'' = TypeEnv (Map.insert x t' env')
        (s2,t2) <- ti (apply s1 env'') e2
        return (s1 `composeSubst` s2, t2)

typeInference :: Map.Map String Scheme -> Exp -> TI Type
typeInference env e = do
        (s,t) <- ti (TypeEnv env) e
        return $ head . drop 100 $ iterate (apply s) t

infer :: Exp -> IO Type
infer e = do
        (res,_) <- runTI (typeInference (Map.fromList startSet) e)
        case res of
            Left err -> error $ "Inference error: " ++ show err
            Right t -> return t

processExpression :: String -> IO String
processExpression s = infer (genTree s) >>= return . outputType
    where genTree s = case parse parseExp "" s of
                        Left err -> error $ "parse error: " ++ show err
                        Right xs -> xs

testExpressions = mapM_ (\x -> processExpression x >>= putStrLn) expressions
    where expressions = [ "let x = fun a -> a in x"
                        , "fun x y -> plus(choose(x, y), one)"
                        , "fun x y z d -> plus(choose(x, choose(z, d)), y)"
                        , "fun x y z d -> plus(x, choose(y, choose(z, d)))"
                        , "fun x y z d -> plus(choose(x, choose(y, z)), d)"
                        , "fun x y z -> choose(choose(x, y), z)"
                        , "fun x y z -> choose(x, choose(y, z))"
                        , "fun x y -> x(map(second, y))"
                        ]

outputType :: Type -> String
outputType t = quantified t ++ traverse (mapper t) t
    where
        quantified t = if length u > 0 then "forall[" ++ nubTypes t ++ "] " else ""
            where u = nubTypes t

        mapper t = Map.fromList $ zip (nub $ allTypes t) ['a'..]

        nubTypes = intersperse ' ' . map (m Map.!) . nub . allTypes
            where m = mapper t

        allTypes (TVar n) = [n]
        allTypes (TFun a b) = allTypes a ++ allTypes b
        allTypes (TTup l) = concat $ map allTypes l
        allTypes (TList t) = allTypes t
        allTypes (TPair t1 t2) = allTypes t1 ++ allTypes t2
        allTypes _ = []

        traverse m (TVar n) = m Map.! n : ""
        traverse m TBool = "bool"
        traverse m TInt = "int"
        traverse m (TList t) = "list[" ++ traverse m t ++ "]"
        traverse m (TPair t1 t2) = "pair[" ++ traverse m t1 ++ ", " ++ traverse m t2 ++ "]"
        traverse m (TFun a@(TFun _ _) b) = "(" ++ traverse m a ++ ") -> " ++ traverse m b
        traverse m (TFun a b) = traverse m a ++ " -> " ++ traverse m b
        traverse m (TTup l) = "(" ++ result ++ ")"
            where result = intercalate ", " $ map (traverse m) l

main = getContents >>= processExpression >>= putStrLn

startSet = [ ("id", Scheme ["a"] (TFun (TVar "a") (TVar "a")))
           , ("one", Scheme [] TInt)
           , ("zero", Scheme [] TInt)
           , ("succ", Scheme [] (TFun TInt TInt))
           , ("plus", Scheme [] (TFun (TTup [TInt, TInt]) TInt))
           , ("choose", Scheme ["a"] (TFun (TTup [TVar "a", TVar "a"]) (TVar "a")))
           , ("not", Scheme [] (TFun TBool TBool))
           , ("const", Scheme ["a","b"] (TFun (TVar "a") (TVar "b")))
           , ("apply", Scheme ["a","b"] (TFun (TTup [TFun (TVar "a") (TVar "b"), TVar "a"]) (TVar "b")))
           , ("choose_curry", Scheme ["a"] (TFun (TVar "a") (TFun (TVar "a") (TVar "a"))))
           , ("eq", Scheme ["a"] (TFun (TTup [TVar "a", TVar "a"]) TBool))
           , ("eq_curry", Scheme ["a"] (TFun (TVar "a") (TFun (TVar "a") TBool)))
           , ("apply_curry", Scheme ["a", "b"] (TFun (TFun (TVar "a") (TVar "b")) (TFun (TVar "a") (TVar "b"))))
           , ("nil", Scheme ["a"] (TList (TVar "a")))
           , ("head", Scheme ["a"] (TFun (TList (TVar "a")) (TVar "a")))
           , ("tail", Scheme ["a"] (TFun (TList (TVar "a")) (TList (TVar "a"))))
           , ("cons", Scheme ["a"] (TFun (TTup [TVar "a", TList (TVar "a")]) (TList (TVar "a"))))
           , ("cons_curry", Scheme ["a"] (TFun (TVar "a") (TFun (TList (TVar "a")) (TList (TVar "a")))))
           , ("map", Scheme ["a", "b"] (TFun (TTup [TFun (TVar "a") (TVar "b"), TList (TVar "a")]) (TList (TVar "b"))))
           , ("map_curry", Scheme ["a"] (TFun (TFun (TVar "a") (TVar "b")) (TFun (TList (TVar "a")) (TList (TVar "b")))))
           , ("pair", Scheme ["a", "b"] (TFun (TTup [TVar "a", TVar "b"]) (TPair (TVar "a") (TVar "b"))))
           , ("pair_curry", Scheme ["a", "b"] (TFun (TVar "a") (TFun (TVar "b") (TPair (TVar "a") (TVar "b")))))
           , ("first", Scheme ["a", "b"] (TFun (TPair (TVar "a") (TVar "b")) (TVar "a")))
           , ("second", Scheme ["a", "b"] (TFun (TPair (TVar "a") (TVar "b")) (TVar "b")))
           ]
